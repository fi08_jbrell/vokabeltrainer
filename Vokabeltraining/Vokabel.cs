﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Vokabeltraining
{
    public partial class Vokabel : Form
    {
        private string[] german;
        private string[] english;
        private string[] espanol;

        private string[] questionArray;
        private string[] answerArray;

        private int[] randomIndices;
        private System.Random randomObject = new Random();

        private int counter;
        private int matches;
        private bool testIsRunning = false;


        public Vokabel()
        {
            InitializeComponent();

            openFileDialog1.ShowDialog();

            try
            {
                string[] lines = System.IO.File.ReadAllLines(openFileDialog1.FileName);

                german = new string[lines.Length / 3];
                english = new string[lines.Length / 3];
                espanol = new string[lines.Length / 3];
                randomIndices = new int[lines.Length / 3];

                for (int i = 0; i < lines.Length; i += 3)
                {
                    german[i / 3] = lines[i];
                    english[i / 3] = lines[i + 1];
                    espanol[i / 3] = lines[i + 2];
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    $"Schwerer Ausnahmefehler:\n{e.Message}\nBitte kontaktieren Sie Ihren Systemadministrator."
                );
                this.Close();
            }
        }

        private void RandomizeIndices()
        {
            randomIndices = Enumerable
                    .Range(0, randomIndices.Length)
                    .OrderBy(_ => randomObject.Next())
                    .ToArray()
                ;
        }

        private bool AssignQuestionsAndAnswers()
        {
            var result = true;
            switch (comboBox_selectLang.SelectedItem)
            {
                case "Deutsch - Englisch":
                    questionArray = german;
                    answerArray = english;
                    break;
                case "Englisch - Deutsch":
                    questionArray = english;
                    answerArray = german;
                    break;
                case "Deutsch - Spanisch":
                    questionArray = german;
                    answerArray = espanol;
                    break;
                case "Spanisch - Deutsch":
                    questionArray = espanol;
                    answerArray = german;
                    break;
                case "Englisch - Spanisch":
                    questionArray = english;
                    answerArray = espanol;
                    break;
                case "Spanisch - Englisch":
                    questionArray = espanol;
                    answerArray = english;
                    break;
                default:
                    result = false;
                    break;
            }

            return result;
        }

        private void TestStart()
        {
            RandomizeIndices();
            counter = 0;
            matches = 0;
            btn_start.Enabled = false;
            textBox_answer.Enabled = true;
            AcceptButton = btn_continue;
            btn_Cancel.Text = "Abbrechen";

            textBox_question.Text = questionArray[randomIndices[counter]];
            textBox_answer.Focus();

            progressBar1.Minimum = 0;
            progressBar1.Maximum = questionArray.Length;

            testIsRunning = true;
        }

        private void TestEnd()
        {
            MessageBox.Show($"Testergebnis: {matches}/{questionArray.Length}");
            btn_continue.Enabled = false;
            textBox_answer.Enabled = false;
            textBox_answer.Text = "";
            textBox_question.Text = "";
            btn_start.Enabled = true;
            progressBar1.Value = 0;
            AcceptButton = btn_start;
            btn_Cancel.Text = "Beenden";

            testIsRunning = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TopMost = true;
            this.Activate();
            comboBox_selectLang.Focus();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void textBox_answer_TextChanged(object sender, EventArgs e)
        {
            btn_continue.Enabled = !String.IsNullOrEmpty(textBox_answer.Text);
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            if (AssignQuestionsAndAnswers())
            {
                TestStart();
            }
            else
            {
                MessageBox.Show("Bitte wählen Sie die Sprachen aus.");
            }
        }

        private void btn_continue_Click(object sender, EventArgs e)
        {
            if (textBox_answer.Text == answerArray[randomIndices[counter]])
            {
                matches++;
                MessageBox.Show("Richtig");
            }
            else
            {
                MessageBox.Show("Falsch");
            }

            counter++;
            progressBar1.Increment(1);

            if (counter == randomIndices.Length)
            {
                TestEnd();
            }
            else
            {
                textBox_question.Text = questionArray[randomIndices[counter]];
                textBox_answer.Text = "";
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            if (testIsRunning)
            {
                TestEnd();
            }
            else
            {
                this.Close();
            }
        }
    }
}